#include "sleepwidget.h"
#include "ui_sleepwidget.h"
#include "OpenRGBSleepPlugin.h"

SleepWidget::SleepWidget(OpenRGBSleepPlugin* plugin, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SleepWidget)
{
    ui->setupUi(this);

    QList<QString> profileList;
    std::vector<std::string> profiles = OpenRGBSleepPlugin::RMPointer->GetProfileManager()->profile_list;
    std::transform(profiles.begin(), profiles.end(), std::back_inserter(profileList), [](const std::string &v){ return QString::fromStdString(v); });
    profileList.insert(0, "None");
    ui->sleepProfileComboBox->addItems(profileList);
    ui->wakeProfileComboBox->addItems(profileList);

    connect(ui->sleepProfileComboBox, QOverload<int>::of(&QComboBox::currentIndexChanged), this, [=](int index) {
        if (index == 0) plugin->SleepProfile = QString();
        else plugin->SleepProfile = profileList.at(index);
    });

    connect(ui->wakeProfileComboBox, QOverload<int>::of(&QComboBox::currentIndexChanged), this, [=](int index) {
        if (index == 0) plugin->WakeProfile = QString();
        else plugin->WakeProfile = profileList.at(index);
    });
}

SleepWidget::~SleepWidget()
{
    delete ui;
}
