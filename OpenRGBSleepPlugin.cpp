#include "OpenRGBSleepPlugin.h"
#include "sleepwidget.h"
#include <QHBoxLayout>
#include <QDBusConnection>

bool OpenRGBSleepPlugin::DarkTheme = false;
ResourceManager* OpenRGBSleepPlugin::RMPointer = nullptr;

OpenRGBPluginInfo OpenRGBSleepPlugin::GetPluginInfo()
{
    printf("[OpenRGBSleepPlugin] Loading plugin info.\n");

    OpenRGBPluginInfo info;
    info.Name         = "Sample plugin";
    info.Description  = "Allo allo";
    info.Version  = VERSION_STRING;
    info.Commit  = GIT_COMMIT_ID;
    info.URL  = "https://gitlab.com/OpenRGBDevelopers/sample-plugin";
    info.Icon.load(":/OpenRGBSleepPlugin.png");

    info.Location     =  OPENRGB_PLUGIN_LOCATION_TOP;
    info.Label        =  "Sleep Detect";
    info.TabIconString        =  "Sleep detection plugin";
    info.TabIcon.load(":/OpenRGBSamplePlugin.png");

    return info;
}

unsigned int OpenRGBSleepPlugin::GetPluginAPIVersion()
{
    printf("[OpenRGBSleepPlugin] Loading plugin API version.\n");

    return OPENRGB_PLUGIN_API_VERSION;
}

void OpenRGBSleepPlugin::Load(bool dark_theme, ResourceManager* resource_manager_ptr)
{
    printf("[OpenRGBSleepPlugin] Loading plugin.\n");

    RMPointer                = resource_manager_ptr;
    DarkTheme                = dark_theme;

    QDBusConnection::systemBus().connect("org.freedesktop.login1", "/org/freedesktop/login1", "org.freedesktop.login1.Manager", "PrepareForSleep", this, SLOT(setSleep(bool)));
}

void OpenRGBSleepPlugin::setSleep(bool sleep)
{
    if (sleep && !SleepProfile.isNull())
    {
        RMPointer->GetProfileManager()->LoadProfile(SleepProfile.toStdString());
    }
    else if (!WakeProfile.isNull())
    {
        RMPointer->GetProfileManager()->LoadProfile(WakeProfile.toStdString());
    }

    for(RGBController* controller : RMPointer->GetRGBControllers())
    {
        controller->UpdateLEDs();
    }
}


QWidget* OpenRGBSleepPlugin::GetWidget()
{
    printf("[OpenRGBSleepPlugin] Creating widget.\n");

    return new SleepWidget(this);
}

QMenu* OpenRGBSleepPlugin::GetTrayMenu()
{
    printf("[OpenRGBSleepPlugin] Creating tray menu.\n");

    return nullptr;
}

void OpenRGBSleepPlugin::Unload()
{
    printf("[OpenRGBSleepPlugin] Time to call some cleaning stuff.\n");
    QDBusConnection::systemBus().disconnect("org.freedesktop.login1", "/org/freedesktop/login1", "org.freedesktop.login1.Manager", "PrepareForSleep", this, SLOT(setSleep(bool)));
}

OpenRGBSleepPlugin::OpenRGBSleepPlugin()
{
    printf("[OpenRGBSleepPlugin] Constructor.\n");
}

OpenRGBSleepPlugin::~OpenRGBSleepPlugin()
{
     printf("[OpenRGBSleepPlugin] Time to free some memory.\n");
}

