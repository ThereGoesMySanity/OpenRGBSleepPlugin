# Sample Plugin 

[![pipeline status](https://gitlab.com/OpenRGBDevelopers/OpenRGBSamplePlugin/badges/master/pipeline.svg)](https://gitlab.com/OpenRGBDevelopers/OpenRGBSamplePlugin/-/commits/master)

## What is this?

This is some boilerplate code that ease plugin writing for [OpenRGB](https://gitlab.com/CalcProgrammer1/OpenRGB).

## Downloads

* [Windows 32](https://gitlab.com/OpenRGBDevelopers/OpenRGBSamplePlugin/-/jobs/artifacts/master/download?job=Windows%2032)
* [Windows 64](https://gitlab.com/OpenRGBDevelopers/OpenRGBSamplePlugin/-/jobs/artifacts/master/download?job=Windows%2064)
* [Linux 32](https://gitlab.com/OpenRGBDevelopers/OpenRGBSamplePlugin/-/jobs/artifacts/master/download?job=Linux%2032)
* [Linux 64](https://gitlab.com/OpenRGBDevelopers/OpenRGBSamplePlugin/-/jobs/artifacts/master/download?job=Linux%2064)
* [MacOS ARM64](https://gitlab.com/OpenRGBDevelopers/OpenRGBSamplePlugin/-/jobs/artifacts/master/download?job=MacOS%20ARM64)
* [MacOS Intel](https://gitlab.com/OpenRGBDevelopers/OpenRGBSamplePlugin/-/jobs/artifacts/master/download?job=MacOS%20Intel)

## How do I install it?

* Download and extract the correct files depending on your system
* Launch OpenRGB
* From the Settings -> Plugins menu, click the "Install plugin" button

## Making your own plugin

If you want to write your own plugin, follow this [guide](./CONTRIBUTING.md)
